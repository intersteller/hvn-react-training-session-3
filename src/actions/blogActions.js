import axios from "axios";
import {
  REQUEST_BLOG_START,
  REQUEST_BLOG_FINISHED,
  REQUEST_BLOG_FAILED
} from "../constant";

const client = axios.create({
  baseURL: 'http://localhost:3000',
  /*headers: {
    'Authorization': `Bearer ${token}`
  }*/
});

export const getBlog = () => {
  return async (dispatch, getFirebase) => {
    try {
      dispatch(requestBlogAction());
      const postData = await client.get("/mockdata/blog.json");
      dispatch(requestSuccessAction(postData.data));
    }
    catch (err) {
      dispatch(requestFailedAction())
    }
  };
};

const requestBlogAction = () => {
  return {
    type: REQUEST_BLOG_START
  }
};

const requestSuccessAction = (data) => {
  return {
    type: REQUEST_BLOG_FINISHED,
    payload: data
  }
};

const requestFailedAction = () => {
  return {
    type: REQUEST_BLOG_FAILED,
    message: "Loading data error!"
  }
};
