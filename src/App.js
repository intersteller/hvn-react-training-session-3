import React from 'react';
import {
  Switch,
  Route
} from "react-router-dom";

import Homepage from "./screens/Homepage";
import AboutUs from "./screens/AboutUs";
import ContactUs from "./screens/ContactUs";
import Blog from "./screens/Blog";
import PostContent from "./screens/Blog/PostContent";
import NotFound from "./screens/NotFound";
import Header from "./partials/Header";
import Footer from "./partials/Footer";
import './style/css/custom.css';

function App() {
  return (
    <div className="App">
      <Header />
        <Switch>
          <Route exact path="/" component={Homepage} />
          <Route exact path="/about-us" component={AboutUs} />
          <Route exact path="/contact-us" component={ContactUs} />
          <Route exact path="/blog" component={Blog} />
          <Route path="/post/:id" component={PostContent} />
          <Route path="*" component={NotFound} />
        </Switch>
      <Footer />
    </div>
  );
}

export default App;
